#----------------------------------------------------------------
# Generated CMake target import file for configuration "RelWithDebInfo".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "UserAnalysis::dump_TCAL1" for configuration "RelWithDebInfo"
set_property(TARGET UserAnalysis::dump_TCAL1 APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(UserAnalysis::dump_TCAL1 PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/dump_TCAL1"
  )

list(APPEND _IMPORT_CHECK_TARGETS UserAnalysis::dump_TCAL1 )
list(APPEND _IMPORT_CHECK_FILES_FOR_UserAnalysis::dump_TCAL1 "${_IMPORT_PREFIX}/bin/dump_TCAL1" )

# Import target "UserAnalysis::MuonAnalysis" for configuration "RelWithDebInfo"
set_property(TARGET UserAnalysis::MuonAnalysis APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(UserAnalysis::MuonAnalysis PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/MuonAnalysis"
  )

list(APPEND _IMPORT_CHECK_TARGETS UserAnalysis::MuonAnalysis )
list(APPEND _IMPORT_CHECK_FILES_FOR_UserAnalysis::MuonAnalysis "${_IMPORT_PREFIX}/bin/MuonAnalysis" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
