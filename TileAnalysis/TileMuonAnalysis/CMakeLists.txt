# The name of the package:
atlas_subdir (TileMuonAnalysis)

# add the defined binaries:
atlas_add_executable(dump_TCAL1 
                     util/dump_TCAL1.cxx
                     LINK_LIBRARIES xAODRootAccess xAODMuon xAODTruth xAODEventInfo xAODMissingET)

# add the defined binaries:
atlas_add_executable(MuonAnalysis 
                     util/MuonAnalysis.cxx
                     LINK_LIBRARIES xAODRootAccess xAODMuon xAODTruth xAODEventInfo xAODMissingET)


