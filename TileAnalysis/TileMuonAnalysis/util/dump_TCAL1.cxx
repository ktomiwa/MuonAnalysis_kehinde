// ROOT include(s):
#include <TChain.h>
#include <TFile.h>
#include <TError.h>

// xAOD include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"
#include "xAODRootAccess/tools/Message.h"

// EDM include(s):
#include "xAODMuon/MuonContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMissingET/MissingETContainer.h"

#include <vector>
#include <string>
#include <sstream>

int main(int argc, char **argv) {

  // The application's name:
  static const char* APP_NAME = "dump_TCAL1";

  if (argc != 2) {
    Error( APP_NAME, "Excatly one argument should be provided with input file names seperated by comma!");
    Error( APP_NAME, "\t... exiting!");
    return -1;
  }

  std::vector<std::string> inputFiles;

  Info( APP_NAME, "Input files:");

  std::stringstream ss(argv[1]);
  std::string inputFile;
  while(std::getline(ss, inputFile, ',')) {
    inputFiles.push_back(inputFile);
    Info( APP_NAME, "\t %s", inputFile.c_str());
  }


  // Initialise the environment for xAOD reading:
  RETURN_CHECK( APP_NAME, xAOD::Init( APP_NAME ) );

  // Create a TChain with the input file(s):
  TChain chain( "CollectionTree" );
  for (std::string input_file : inputFiles) {
    chain.Add( inputFile.c_str() );  
  }

  // Create a TEvent object with this TChain as input:
  xAOD::TEvent event;
  RETURN_CHECK( APP_NAME, event.readFrom( &chain ) );
  
  // Loop over the input file(s):
  const ::Long64_t entries = event.getEntries();
  for( ::Long64_t entry = 0; entry < entries; ++entry ) {
    
    // Load the event:
    if( event.getEntry( entry ) < 0 ) {
      Error( APP_NAME, XAOD_MESSAGE( "Failed to load entry %i" ),
             static_cast< int >( entry ) );
      return 1;
    }
    
    const xAOD::EventInfo* eventInfo = 0;
    RETURN_CHECK(APP_NAME, event.retrieve(eventInfo, "EventInfo"));

    const int runNumber = eventInfo->runNumber();
    const int lumiBlock = eventInfo->lumiBlock();
    const ::Long64_t eventNumber = eventInfo->eventNumber();
    const float actualInteractionsPerXing = eventInfo->actualInteractionsPerCrossing();
    const float averagelInteractionsPerXing = eventInfo->averageInteractionsPerCrossing();
      
    Info( APP_NAME, "Processed run: %i, lumiblock: %i, event number: %i, actual mu: %f, average mu: %f",
          static_cast< int >(runNumber),
          static_cast< int >(lumiBlock),
          static_cast< int >(eventNumber),
          actualInteractionsPerXing,
          averagelInteractionsPerXing);


    const xAOD::MissingETContainer* met_ref = 0;
    RETURN_CHECK(APP_NAME, event.retrieve(met_ref, "MET_Reference_AntiKt4LCTopo"));

    const xAOD::MissingET* finalClus = (*met_ref)["FinalClus"];
    const double finalClus_mpx = finalClus->mpx();
    const double finalClus_mpy = finalClus->mpy();
    const double finalClus_met = finalClus->met();
    const double finalClus_sumet = finalClus->sumet();

    Info( APP_NAME, "  missing et information from FinalClus: mpx/mpy/met/sumet => %f / %f / %f / %f",
          finalClus_mpx,
          finalClus_mpy,
          finalClus_met,
          finalClus_sumet);

    // Load the muons from it:
    const xAOD::MuonContainer* muons = 0;
    RETURN_CHECK( APP_NAME, event.retrieve( muons, "Muons" ) );

    Info( APP_NAME, "Number of muons in event %i: %i",
          static_cast< int >( entry ),
          static_cast< int >( muons->size() ) );


    int muon_idx(0);

    // Iterate over all muons
    for (const xAOD::Muon*  muon : *muons) {

      ++muon_idx;

      float muPtCone40 = muon->auxdata<float>("ptcone40");
      Info (APP_NAME, "Muon %i\t Pt:%f eta:%f phi:%f", muon_idx, muon->pt(), muon->eta(), muon->phi());
      Info( APP_NAME, "PtCone40: %f", muPtCone40);


      if (!muon->isAvailable< std::vector< float > >( "TCAL1_cells_energy" )) {
        Warning(APP_NAME, "TCAL1 variables are not available!!!");
        continue;
      }

      // Get energy of Tile cells around muon track
      std::vector<float> cells_energy = muon->auxdata< std::vector< float > >( "TCAL1_cells_energy" );
      std::vector<float> cells_eta = muon->auxdata< std::vector< float > >( "TCAL1_cells_eta" );
      std::vector<float> cells_phi = muon->auxdata< std::vector< float > >( "TCAL1_cells_phi" );

      //      std::vector<float> cells_x = muon->auxdata< std::vector< float > >( "TCAL1_cells_x" );

      //      std::vector<float> cells_muon_x = muon->auxdata< std::vector< float > >( "TCAL1_cells_muon_x" );
      std::vector<float> cells_muon_dx = muon->auxdata< std::vector< float > >( "TCAL1_cells_muon_dx" );
      std::vector<float> cells_muon_dedx = muon->auxdata< std::vector< float > >( "TCAL1_cells_muon_dedx" );

      std::vector<int> cells_side = muon->auxdata< std::vector< int > >( "TCAL1_cells_side" );
      std::vector<unsigned short> cells_section = muon->auxdata< std::vector< unsigned short > >( "TCAL1_cells_section" );
      std::vector<unsigned short> cells_module = muon->auxdata< std::vector< unsigned short > >( "TCAL1_cells_module" );
      std::vector<unsigned short> cells_tower = muon->auxdata< std::vector< unsigned short > >( "TCAL1_cells_tower" );
      std::vector<unsigned short> cells_sample = muon->auxdata< std::vector< unsigned short > >( "TCAL1_cells_sample" );


      int n_cells = cells_energy.size();
      // Iterrate over Tile cells around muon track
      for (int i = 0; i < n_cells; ++i) {
        float cell_energy = cells_energy[i];

        float cell_eta = cells_eta[i];
        float cell_phi = cells_phi[i];

        //        float cell_x = cells_x[i];

        //        float cell_muon_x = cells_muon_x[i];
        float cell_muon_dx = cells_muon_dx[i];
        float cell_muon_dedx = cells_muon_dedx[i];

        int cell_side = cells_side[i];
        unsigned short cell_section = cells_section[i];
        unsigned short cell_module = cells_module[i];
        unsigned short cell_tower = cells_tower[i];
        unsigned short cell_sample = cells_sample[i];

        Info( APP_NAME, "  cell %i\t eta:%4.1f phi:%4.1f side:%2i section:%i module:%2i tower:%2i sample:%i",
              (i + 1),
              cell_eta,
              cell_phi,
              cell_side,
              static_cast< int >( cell_section ),
              static_cast< int >( cell_module ),
              static_cast< int >( cell_tower ),
              static_cast< int >( cell_sample ));


        Info( APP_NAME, "\t\t energy:%6.1f [MeV] muon dx:%5.1f [mm] de/dx:%5.2f [MeV/mm]",
              cell_energy,
              cell_muon_dx, 
              cell_muon_dedx);

      }
      
    }
  }
  

  // Return gracefully:
  return 0;
}
