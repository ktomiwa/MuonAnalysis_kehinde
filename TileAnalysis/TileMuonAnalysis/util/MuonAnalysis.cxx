// ROOT include(s):
#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TError.h>
#include <TGraph.h>
#include <TProfile.h>

// xAOD include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"
#include "xAODRootAccess/tools/Message.h"

// EDM include(s):
#include "xAODMuon/MuonContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMissingET/MissingETContainer.h"

#include <vector>
#include <string>
#include <sstream>
#include <fstream>

int main(int argc, char **argv) {

  // The application's name:
  static const char* APP_NAME = "MuonAnalysis";

  if (argc < 2) {
    Error( APP_NAME, "Excatly one argument should be provided with input file names seperated by comma!");
    Error( APP_NAME, "\t... exiting!");
    return -1;
  }

  std::vector<std::string> inputFiles;

  //Info( APP_NAME, "Input files:");

  std::stringstream ss(argv[1]);
  std::string inputFile;
  TString output = argv[2];
  bool list  =   atoi(argv[3]);
  bool docut =   atoi(argv[4]);
  if(list){
    std::ifstream file1(argv[1]);
    while(getline(file1,inputFile)){
      inputFiles.push_back(inputFile);
      Info( APP_NAME, "\t %s", inputFile.c_str());
    }
  }else{
     while(std::getline(ss, inputFile, ',')) {
       inputFiles.push_back(inputFile);
       Info( APP_NAME, "\t %s", inputFile.c_str());
     }
  }
   // Create a TChain with the input file(s):
  //std::cout<<"chaining input....";
  TChain chain( "CollectionTree" );
  for (std::string input_file : inputFiles) {
   // Info( APP_NAME, "Adding %s", input_file.c_str());
    TFile *file = TFile::Open(input_file.c_str());
    if(!((TTree*)file->Get("CollectionTree"))) continue;
    std::cout<<"chain: "<<input_file.c_str()<<std::endl;
    chain.Add( input_file.c_str() );

  }


  //const char* outputFileName = "muon_analysis.root";
  TFile* outputFile = TFile::Open(output, "recreate");


  TH1F* hn_cutflow = new TH1F("cutflow", "cutflow", 9,0,9);

  TAxis *axis = hn_cutflow ->GetXaxis();
  std::vector<TString> binLabels = {"All Event", "Muon==1", "nPSHit >=8","|#{d_0}|<=380", "|#{z_0}|<=800", "10< MuonP <30",
	                        "dE > 60", "dphiMuonCell < 0.05", "celMuondx >200"
	                       };
  for (size_t i=0;i<binLabels.size();++i) axis->SetBinLabel(i+1,binLabels[i]);

  std::map<TString, TH1F*> m_histoTH1F;
  std::map<TString, TH2F*> m_histoTH2F;
  std::map<TString, TProfile*> m_histoTProfile;

  TH1F* hn_muon = new TH1F("number of muons", "number of muons", 10,-0.5,9.5);
  TH1F* hn_track = new TH1F("number of track", "number of track", 20,-0.5,20.5);
  TH1F* hn_cells = new TH1F("number of cells", "number of cells", 30,-0.5,30);
  TH1F* hn_modules = new TH1F("number of modules", "number of modules", 63,-0.5,63.5);
  TH1F* hz_0;
  TH1F* hd_0;
  TH1F* hnPH;
  TH1F* hnSH;
  TH1F* hnPSH;
  TH1F* hmuon_p;
  if (!docut){
      hz_0 = new TH1F("z_0", "z_0", 100,0,1000);
      hd_0 = new TH1F("d_0", "d_0",100,0,1000);
      hnPH = new TH1F("nPH", "nPH", 20,-0.5,20);
      hnSH = new TH1F("nSH", "nSH", 20,-0.5,20);
      hnPSH = new TH1F("nPSH", "nPSH", 40,-0.5,40);
      hmuon_p = new TH1F("hmuon_p", "hmuon_p",100,0,100);

  }else{
       hz_0 = new TH1F("z_0", "z_0", 80,0,800);
       hd_0 = new TH1F("d_0", "d_0",40,0,400);
       hnPH = new TH1F("nPH", "nPH", 20,-0.5,20);
       hnSH = new TH1F("nSH", "nSH", 20,-0.5,20);
       hnPSH = new TH1F("nPSH", "nPSH", 40,-0.5,40);
       hmuon_p = new TH1F("hmuon_p", "hmuon_p",20,10,30);
  }

  TH1F* hn_muon_theta = new TH1F("n_muon_theta", "n_muon_theta", 60,-3.1415926,3.1415926);
  TH1F* hn_muon_phi = new TH1F("n_muon_phi", "n_muon_phi", 60,-3.0,3.0);




  // cell based plots
  for(TString layer:{"LayerA","LayerBC","LayerD"}){
     m_histoTH1F["CellEnergy_"+layer]    = new TH1F("CellEnergy_"+layer, "CellEnergy_"+layer, 50, 0, 2500);
     m_histoTH1F["celleta_"+layer]       = new TH1F("celleta_"+layer, "celleta_"+layer, 60,-1.7,1.7);
     m_histoTH1F["cellphi_"+layer]       = new TH1F("cellphi_"+layer, "cellphi_"+layer, 60,-3.0,3.0);
     m_histoTH1F["cellx_"+layer]         = new TH1F("cellx_"+layer, "cellx_"+layer, 100,-4000,4000);
     m_histoTH1F["cellmuon_x_"+layer]    = new TH1F("cellmuon_x_"+layer, "cellmuon_x_"+layer, 10,-5,5);
     m_histoTH1F["dphiMuonCell_"+layer]   = new TH1F("dphiMuonCell_"+layer, "dphiMuonCell"+layer, 20,0,3.1415926);
     m_histoTH1F["cellmuon_dx_"+layer]   = new TH1F("cellmuon_dx_"+layer, "cellmuon_dx_"+layer, 100,0,1000);
     m_histoTH1F["cellmuon_dedx_"+layer] = new TH1F("cellmuon_dedx_"+layer, "cellmuon_dedx_"+layer, 50,0,5);
  }
  for(TString sample:{"LB","EB"}){
    for(TString layer:{"LayerA","LayerBC","LayerD"}){
       //if(sample == "EB") {layer = "LayerB";}
       m_histoTH1F["cellmuon_dedx_"+sample+"_"+layer] = new TH1F("cellmuon_dedx_"+sample+"_"+layer, "cellmuon_dedx_"+sample+"_"+layer, 50,0,5);
     }
  }
  TH1* hCellEnergy = new TH1F("hCellEnergy", "Energy of Tile cells around muon tracks", 50, 0, 2500);
  TH1F* hc_eta = new TH1F("c_eta", "c_eta", 60,-1.7,1.7);
  TH1F* hc_phi = new TH1F("c_phi", "c_phi", 60,-3.0,3.0);
  TH1F* hn_cells_x = new TH1F("n_cells_x ", "n_cells_x ", 100,-4000,4000);
  TH1F* hn_muon_x = new TH1F("n_muon_x", "n_muon_x", 10,-5,5);
  TH1F* hn_muon_dx = new TH1F("n_muon_dx", "n_muon_dx", 100,0,1000);
  TH1F* hn_dphiMuonCell = new TH1F("n_dphiMuonCell", "n_dphiMuonCell", 20,0,3.1415926);

  TH1F* hn_muon_dedx = new TH1F("n_muon_dedx", "n_muon_dedx", 50,0,5);
  TH1F* hn_muon_dedx_LB_A3 = new TH1F("n_muon_dedx_LB_A3", "n_muon_dedx_LB_A3", 50,0,5);
  TH1F* hn_muon_dedx_LB_D2 = new TH1F("n_muon_dedx_LB_D2", "n_muon_dedx_LB_D2", 50,0,5);

  TProfile *hprof  = new TProfile("hprof","dE vs dx",100,0,1000,0,4500);
  TH2F *h2dEvsdx = new TH2F("dEvsdx","dEvsdx", 100,0,1000,100,0,4500);
  TH2F *h2dEvsdx_LB_BC = new TH2F("dEvsdx_LB_BC","dEvsdx_LB_BC", 100,0,1000,100,0,4500);
  TH2F *h2dEvsCellphi = new TH2F("dEvsCellphi","dEvsCellphi", 100,-2.0,-1.5,100,0,4500);
  TH2F *h2dEvsCellphi_LB_BC = new TH2F("dEvsCellphi_LB_BC","dEvsCellphi_LB_BC", 100,-2.0,-1.5,100,0,4500);

  TH2F *h2dEvsCellphi_LB_BC1 = new TH2F("dEvsCellphi_LB_BC1","dEvsCellphi_LB_BC1", 100,-2.0,-1.5,100,0,4500);
  TH2F *h2dEvsCellphi_LB_BC2 = new TH2F("dEvsCellphi_LB_BC2","dEvsCellphi_LB_BC2", 100,-2.0,-1.5,100,0,4500);
  TH2F *h2dEvsCellphi_LB_BC3 = new TH2F("dEvsCellphi_LB_BC3","dEvsCellphi_LB_BC3", 100,-2.0,-1.5,100,0,4500);
  TH2F *h2dEvsCellphi_LB_BC4 = new TH2F("dEvsCellphi_LB_BC4","dEvsCellphi_LB_BC4", 100,-2.0,-1.5,100,0,4500);
  TH2F *h2dEvsCellphi_LB_BC5 = new TH2F("dEvsCellphi_LB_BC5","dEvsCellphi_LB_BC5", 100,-2.0,-1.5,100,0,4500);
  TH2F *h2dEvsCellphi_LB_BC6 = new TH2F("dEvsCellphi_LB_BC6","dEvsCellphi_LB_BC6", 100,-2.0,-1.5,100,0,4500);
  TH2F *h2dEvsCellphi_LB_BC7 = new TH2F("dEvsCellphi_LB_BC7","dEvsCellphi_LB_BC7", 100,-2.0,-1.5,100,0,4500);
  TH2F *h2dEvsCellphi_LB_BC8 = new TH2F("dEvsCellphi_LB_BC8","dEvsCellphi_LB_BC8", 100,-2.0,-1.5,100,0,4500);
  TH2F *h2dEvsCellphi_LB_BC9 = new TH2F("dEvsCellphi_LB_BC9","dEvsCellphi_LB_BC9", 100,-2.0,-1.5,100,0,4500);
  TH2F *h2dEvsCellphi_LB_BC10 = new TH2F("dEvsCellphi_LB_BC10","dEvsCellphi_LB_BC10", 100,-2.0,-1.5,100,0,4500);

  std::vector<double> All_dedx; All_dedx.clear();
  std::vector<double> All_eta; All_eta.clear();
  std::vector<double> All_phi; All_phi.clear();
  TH2F *h2AllTdEvscelleta = new TH2F("AllTdEvscelleta","AllTdEvscelleta",100,-1.7,1.7,1000,0,100);
  TH2F *h2AllTdEvscellphi = new TH2F("AllTdEvscellphi","AllTdEvscellphi",100,-3.0,3.0,1000,0,100);

  std::vector<double> LA_dedx; LA_dedx.clear();
  std::vector<double> LA_eta;  LA_eta.clear();
  std::vector<double> LA_phi;  LA_phi.clear();
  TH2F *h2LATdEvscelleta = new TH2F("LATdEvscelleta","LATdEvscelleta",100,-1.7,1.7,1000,0,100);
  TH2F *h2LATdEvscellphi = new TH2F("LATdEvscellphi","LATdEvscellphi",100,-3.0,3.0,1000,0,100);

  std::vector<double> LBC_dedx; LBC_dedx.clear();
  std::vector<double> LBC_eta;  LBC_eta.clear();
  std::vector<double> LBC_phi;  LBC_phi.clear();
  TH2F *h2LBCTdEvscelleta = new TH2F("LBCTdEvscelleta","LBCTdEvscelleta",100,-1.7,1.7,1000,0,100);
  TH2F *h2LBCTdEvscellphi = new TH2F("LBCTdEvscellphi","LBCTdEvscellphi",100,-3.0,3.0,1000,0,100);

  std::vector<double> LD_dedx; LD_dedx.clear();
  std::vector<double> LD_eta;  LD_eta.clear();
  std::vector<double> LD_phi;  LD_phi.clear();
  TH2F *h2LDTdEvscelleta = new TH2F("LDTdEvscelleta","LDTdEvscelleta",100,-1.7,1.7,1000,0,100);
  TH2F *h2LDTdEvscellphi = new TH2F("LDTdEvscellphi","LDTdEvscellphi",100,-3.0,3.0,1000,0,100);




  // Initialise the environment for xAOD reading:
  RETURN_CHECK( APP_NAME, xAOD::Init( APP_NAME ) );



  // Create a TEvent object with this TChain as input:
  xAOD::TEvent event;
  RETURN_CHECK( APP_NAME, event.readFrom( &chain ) );
   
   
   int count_muons = 0;
  // Loop over the input file(s):
  const ::Long64_t entries = event.getEntries();
  for( ::Long64_t entry = 0; entry < entries; ++entry ) {
     hn_cutflow->Fill(0);
    // Load the event:
    if( event.getEntry( entry ) < 0 ) {
      Error( APP_NAME, XAOD_MESSAGE( "Failed to load entry %i" ),
             static_cast< int >( entry ) );
      //continue;
     // std::cout<<"remove"<<argv[1]<<std::endl;
      return 1;
    }
    //std::cout<<"check entry: "<<entry<<std::endl;

    const xAOD::EventInfo* eventInfo = 0;
    RETURN_CHECK(APP_NAME, event.retrieve(eventInfo, "EventInfo"));

    const int runNumber = eventInfo->runNumber();
    //std::cout<<"run Number: "<<runNumber<<std::endl;
    //break; 
    const int lumiBlock = eventInfo->lumiBlock();
    const ::Long64_t eventNumber = eventInfo->eventNumber();
    const float actualInteractionsPerXing = eventInfo->actualInteractionsPerCrossing();
    const float averagelInteractionsPerXing = eventInfo->averageInteractionsPerCrossing();

    //Info( APP_NAME, "Processed run: %i, lumiblock: %i, event number: %i, actual mu: %f, average mu: %f"
         // , static_cast< int >(runNumber)
         // , static_cast< int >(lumiBlock)
         // , static_cast< int >(eventNumber)
         // , actualInteractionsPerXing
         // , averagelInteractionsPerXing);


   // const xAOD::MissingETContainer* met_ref = 0;
   // RETURN_CHECK(APP_NAME, event.retrieve(met_ref, "MET_Reference_AntiKt4LCTopo"));

    //const xAOD::MissingET* finalClus = (*met_ref)["FinalClus"];
    //const double finalClus_mpx = finalClus->mpx();
    //const double finalClus_mpy = finalClus->mpy();
    //const double finalClus_met = finalClus->met();
    //const double finalClus_sumet = finalClus->sumet();

    //Info( APP_NAME, "\t ... missing et information from FinalClus: mpx/mpy/met/sumet => %f / %f / %f / %f"
    //      , finalClus_mpx
    //      , finalClus_mpy
    //      , finalClus_met
    //      , finalClus_sumet);

    // Load the muons from it:
    const xAOD::MuonContainer* muons = 0;
    RETURN_CHECK( APP_NAME, event.retrieve( muons, "Muons" ) );

   // Info( APP_NAME, "Number of muons in event %i: %i"
         // , static_cast< int >( entry )
         // , static_cast< int >( muons->size() ) );
     //cut 1.
     if(!(muons->size() == 1) && docut) continue;
     hn_cutflow->Fill(1);
     // count muons
     hn_muon->Fill(muons->size());
     //Call track info
    const xAOD::TrackParticleContainer* tracks = 0;
    RETURN_CHECK( APP_NAME, event.retrieve( tracks, "CombinedMuonTrackParticles" ) );
    hn_track->Fill(tracks->size());


    int nPSH = 0;
    for (const xAOD::TrackParticle* track : *tracks ){

        //float          z0            = track->auxdata<float>( "z0" );
        //float          d0            = track->auxdata<float>( "d0" );
        unsigned char  n_PixelHits   = track->auxdata<unsigned char>("numberOfPixelHits");
        unsigned char  n_SCTHits     = track->auxdata<unsigned char>("numberOfSCTHits");

        //check
        //Info( APP_NAME, "nPH: %d, nSH: %d",
                         //  n_PixelHits,
                         //  n_SCTHits);

        nPSH = (unsigned int)n_PixelHits + n_SCTHits;
        hnPH ->Fill((unsigned int)n_PixelHits);
        hnSH ->Fill((unsigned int)n_SCTHits);


  }
  //cut 2.
  if(nPSH < 1)continue;
  if (nPSH<8 && docut )continue;
  hn_cutflow->Fill(2);
  hnPSH ->Fill(nPSH);

    //check
    //std::cout<<"muon pt: "<<muons->p4->Pt()<<std::endl;
    // Iterate over all muons
        count_muons = 0;
                       
        for (const xAOD::Muon*  muon : *muons) {
            if (!muon->isAvailable< std::vector< float > >( "TCAL1_cells_energy" ))continue;
            if(muon->muonType() != xAOD::Muon::Combined) continue;
            count_muons++;
            //xAOD::Muon::Quality my_quality;
            //my_quality = muon->getQuality(**mu_itr);
            //if!((my_quality <= xAOD::Muon::VeryLoose)) continue;

            //std::cout<<"combined muons"<<std::endl;
            float muPtCone40 = muon->auxdata<float>("ptcone40");
            //Info( APP_NAME, "PtCone40: %f", muPtCone40);

            // Get energy of Tile cells around muon track
            std::vector<float> cells_energy = muon->auxdata< std::vector< float > >( "TCAL1_cells_energy" );
            std::vector<float> cells_eta = muon->auxdata< std::vector< float > >( "TCAL1_cells_eta" );
            std::vector<float> cells_phi = muon->auxdata< std::vector< float > >( "TCAL1_cells_phi" );

            std::vector<float> cells_x = muon->auxdata< std::vector< float > >( "TCAL1_cells_x" );

            std::vector<float> cells_muon_x = muon->auxdata< std::vector< float > >( "TCAL1_cells_muon_x" );
            std::vector<float> cells_muon_dx = muon->auxdata< std::vector< float > >( "TCAL1_cells_muon_dx" );
            std::vector<float> cells_muon_dedx = muon->auxdata< std::vector< float > >( "TCAL1_cells_muon_dedx" );

            std::vector<int> cells_side = muon->auxdata< std::vector< int > >( "TCAL1_cells_side" );
            std::vector<unsigned short> cells_section = muon->auxdata< std::vector< unsigned short > >( "TCAL1_cells_section" );
            std::vector<unsigned short> cells_module = muon->auxdata< std::vector< unsigned short > >( "TCAL1_cells_module" );
            std::vector<unsigned short> cells_tower = muon->auxdata< std::vector< unsigned short > >( "TCAL1_cells_tower" );
            std::vector<unsigned short> cells_sample = muon->auxdata< std::vector< unsigned short > >( "TCAL1_cells_sample" );



            float d0 = muon->primaryTrackParticle()->d0();
            float z0 = muon->primaryTrackParticle()->z0();

            // cut 3.
            if(!(fabs(d0)<= 380.0) && docut) continue;
            hn_cutflow->Fill(3);
            // cut 4
            if(!(fabs(z0)<= 800.0) && docut ) continue;
            hn_cutflow->Fill(4);

            //std::cout<<"d0/z0\t"<<fabs(d0)<<"/"<<fabs(z0)<<std::endl;
            hz_0 ->Fill(fabs(z0));
            hd_0 ->Fill(fabs(d0));

            uint8_t numberOfPrecisionLayers1=0;
            if( ! muon->summaryValue (numberOfPrecisionLayers1, xAOD::SummaryType::numberOfPrecisionLayers) )
             //  std::cout<<"numberOfPrecisionLayers not available"<<std::endl;
           // std::cout<<"numberOfPrecisionLayers1"<<(unsigned int)numberOfPrecisionLayers1<<std::endl;

            uint8_t numberOfPrecisionLayers2 = muon->uint8SummaryValue (xAOD::SummaryType::numberOfPrecisionLayers);
            //std::cout<<"numberOfPrecisionLayers2"<<(unsigned int)numberOfPrecisionLayers2<<std::endl;
            //auto nph = -1;
            //auto nsh = -1;
            //muon->primaryTrackParticle()->summaryValue(nph, xAOD::SummaryType::numberOfPixelHits);
            //muon->primaryTrackParticle()->summaryValue(nsh, xAOD::SummaryType::numberOfSCTHits);
           // uint8_t numberOfPrecisionLayers = muon->uint8SummaryValue (xAOD::SummaryType::numberOfPrecisionLayers);
           // std::cout<<"check numberOfPrecisionLayers: "<<numberOfPrecisionLayers<<std::endl;

            //unsigned int  nph = muon->primaryTrackParticle()->numberOfPixelHits();
            //unsigned int  nsh = muon->primaryTrackParticle()->numberOfSCTHits();
            //std::cout<<"nph/nsh\t"<<nph<<"/"<<nsh<<std::endl;




            double muonphi = muon->auxdata<float>( "phi" );
            //float muon_pt = muon->auxdata<float>( "pt" );
           // hmuon_pt->Fill(muon_pt/1000.0);
            TLorentzVector muon4V = muon->p4();
            double muonP = muon4V.P()/1000.0;
           // std::cout<<"muonP: "<<muonP<<std::endl;
            double muontheta = muon4V.Theta();

            //std::cout<<"Muon theta: "<<muontheta<<std::endl;
            hn_muon_theta->Fill(muontheta);
            hn_muon_phi->Fill(muonphi);
            // cut 5.
            if((muonP < 10.0 || muonP > 30.0) &&docut) continue;
            hn_cutflow->Fill(5);
            hmuon_p->Fill(muonP);




            int n_cells = cells_energy.size();
            hn_cells->Fill(cells_energy.size());

            // Iterrate over Tile cells around muon track
            for (int i = 0; i < n_cells; ++i) {

                int cell_side = cells_side[i];
                unsigned short cell_section = cells_section[i];
                unsigned short cell_module = cells_module[i];
                unsigned short cell_tower = cells_tower[i];
                unsigned short cell_sample = cells_sample[i];

                //Select Long Barrel and layer BC and muon_phi [-2.0,-1.5]
                if(cell_section==1 && cell_sample == 2 && muonphi > -2.0 && muonphi < -1.5)hn_modules->Fill(cell_module);
                //std::cout<<"cells sample: "<<cell_sample<<std::endl;
                float cell_energy_NoCut = cells_energy[i];
                float cell_muon_dx_NoCut = cells_muon_dx[i];
                hprof->Fill(cell_muon_dx_NoCut,cell_energy_NoCut);
                h2dEvsdx->Fill(cell_muon_dx_NoCut,cell_energy_NoCut);
                if(cell_section==1 && cell_sample == 1) h2dEvsdx_LB_BC->Fill(cell_muon_dx_NoCut,cell_energy_NoCut);

                float cell_energy = cells_energy[i];
                // cut 6.
                if (cell_energy < 60.0 && docut) continue;
                hn_cutflow->Fill(6);
                hCellEnergy->Fill(cell_energy);
                if(cell_sample == 0)m_histoTH1F["CellEnergy_LayerA"]->Fill(cell_energy);
                if(cell_sample == 1)m_histoTH1F["CellEnergy_LayerBC"]->Fill(cell_energy);
                if(cell_sample == 2)m_histoTH1F["CellEnergy_LayerD"]->Fill(cell_energy);

                float cell_eta = cells_eta[i];
                hc_eta->Fill(cell_eta);
                All_eta.push_back(cell_eta);
                if(cell_sample == 0){m_histoTH1F["celleta_LayerA"]->Fill(cell_eta);LA_eta.push_back(cell_eta);}
                if(cell_sample == 1){m_histoTH1F["celleta_LayerBC"]->Fill(cell_eta);LBC_eta.push_back(cell_eta);}
                if(cell_sample == 2){m_histoTH1F["celleta_LayerD"]->Fill(cell_eta);LD_eta.push_back(cell_eta);}

                float cell_phi = cells_phi[i];
                hc_phi->Fill(cell_phi);
                All_phi.push_back(cell_phi);
                if(cell_sample == 0){m_histoTH1F["cellphi_LayerA"]->Fill(cell_phi);LA_phi.push_back(cell_phi);}
                if(cell_sample == 1){m_histoTH1F["cellphi_LayerBC"]->Fill(cell_phi);LBC_phi.push_back(cell_phi);}
                if(cell_sample == 2){m_histoTH1F["cellphi_LayerD"]->Fill(cell_phi);LD_phi.push_back(cell_phi);}



                double dphiMuonCell = fabs(muonphi - cell_phi);
                // cut 7
                if (dphiMuonCell> 0.045 && docut) continue;
                hn_cutflow->Fill(7);
                hn_dphiMuonCell->Fill(dphiMuonCell);
                if(cell_sample == 0)m_histoTH1F["dphiMuonCell_LayerA"]->Fill(dphiMuonCell);
                if(cell_sample == 1)m_histoTH1F["dphiMuonCell_LayerBC"]->Fill(dphiMuonCell);
                if(cell_sample == 2)m_histoTH1F["dphiMuonCell_LayerD"]->Fill(dphiMuonCell);

                float cell_x = cells_x[i];
                hn_cells_x->Fill(cell_x);
                if(cell_sample == 0)m_histoTH1F["cellx_LayerA"]->Fill(cell_x);
                if(cell_sample == 1)m_histoTH1F["cellx_LayerBC"]->Fill(cell_x);
                if(cell_sample == 2)m_histoTH1F["cellx_LayerD"]->Fill(cell_x);


                float cell_muon_x = cells_muon_x[i];
                hn_muon_x->Fill(cell_muon_x);
                if(cell_sample == 0)m_histoTH1F["cellmuon_x_LayerA"]->Fill(cell_muon_x);
                if(cell_sample == 1)m_histoTH1F["cellmuon_x_LayerBC"]->Fill(cell_muon_x);
                if(cell_sample == 2)m_histoTH1F["cellmuon_x_LayerD"]->Fill(cell_muon_x);

                float cell_muon_dx = cells_muon_dx[i];
                // cut 8.
                if (cell_muon_dx<200.0 && docut) continue;
                hn_cutflow->Fill(8);
                hn_muon_dx->Fill(cell_muon_dx);
                if(cell_sample == 0)m_histoTH1F["cellmuon_dx_LayerA"]->Fill(cell_muon_dx);
                if(cell_sample == 1)m_histoTH1F["cellmuon_dx_LayerBC"]->Fill(cell_muon_dx);
                if(cell_sample == 2)m_histoTH1F["cellmuon_dx_LayerD"]->Fill(cell_muon_dx);

                float cell_muon_dedx = cells_muon_dedx[i];
                hn_muon_dedx->Fill(cell_muon_dedx);
                All_dedx.push_back(cell_muon_dedx);
                if(cell_sample == 0){
                    m_histoTH1F["cellmuon_dedx_LayerA"]->Fill(cell_muon_dedx);
                    LA_dedx.push_back(cell_muon_dedx);
                }
                if(cell_sample == 1){
                    m_histoTH1F["cellmuon_dedx_LayerBC"]->Fill(cell_muon_dedx);
                    LBC_dedx.push_back(cell_muon_dedx);
                }
                if(cell_sample == 2){
                    m_histoTH1F["cellmuon_dedx_LayerD"]->Fill(cell_muon_dedx);
                    LD_dedx.push_back(cell_muon_dedx);
                }
                
                if(cell_section==1){
                  if(cell_sample == 0)m_histoTH1F["cellmuon_dedx_LB_LayerA"]->Fill(cell_muon_dedx);
                  if(cell_sample == 1)m_histoTH1F["cellmuon_dedx_LB_LayerBC"]->Fill(cell_muon_dedx);
                  if(cell_sample == 2)m_histoTH1F["cellmuon_dedx_LB_LayerD"]->Fill(cell_muon_dedx);
                }
                if(cell_section==2){
                  if(cell_sample == 0)m_histoTH1F["cellmuon_dedx_EB_LayerA"]->Fill(cell_muon_dedx);
                  if(cell_sample == 1)m_histoTH1F["cellmuon_dedx_EB_LayerBC"]->Fill(cell_muon_dedx);
                  if(cell_sample == 2)m_histoTH1F["cellmuon_dedx_EB_LayerD"]->Fill(cell_muon_dedx);
                }

                //Fill Barrel A3 and D2
                if(cell_section==1){
                  if(cell_sample == 0 && i == 3)hn_muon_dedx_LB_A3->Fill(cell_muon_dedx);
                  if(cell_sample == 2 && i == 2)hn_muon_dedx_LB_D2->Fill(cell_muon_dedx);

                }


                hprof->Fill(cell_muon_dx,cell_energy);
                //h2dEvsdx->Fill(cell_muon_dx,cell_energy);
                //if(cell_section==1 && cell_sample == 1) h2dEvsdx_LB_BC->Fill(cell_muon_dx,cell_energy);
                h2dEvsCellphi->Fill(muonphi,cell_energy);
                // Select Long Barrel, Layer BC
                if (cell_sample == 1 &&cell_section==1){
                h2dEvsCellphi_LB_BC->Fill(muonphi,cell_energy);
                if(cell_module == 41)h2dEvsCellphi_LB_BC1->Fill(muonphi,cell_energy);
                if(cell_module == 42)h2dEvsCellphi_LB_BC2->Fill(muonphi,cell_energy);
                if(cell_module == 43)h2dEvsCellphi_LB_BC3->Fill(muonphi,cell_energy);
                if(cell_module == 44)h2dEvsCellphi_LB_BC4->Fill(muonphi,cell_energy);
                if(cell_module == 45)h2dEvsCellphi_LB_BC5->Fill(muonphi,cell_energy);
                if(cell_module == 46)h2dEvsCellphi_LB_BC6->Fill(muonphi,cell_energy);
                if(cell_module == 47)h2dEvsCellphi_LB_BC7->Fill(muonphi,cell_energy);
                if(cell_module == 48)h2dEvsCellphi_LB_BC8->Fill(muonphi,cell_energy);
                if(cell_module == 49)h2dEvsCellphi_LB_BC9->Fill(muonphi,cell_energy);
                if(cell_module == 50)h2dEvsCellphi_LB_BC10->Fill(muonphi,cell_energy);
                }

         } // loop overcells

        }//loop over muons
     }//loop over event

     //double averageAll = hn_muon_dedx->Integral()/hn_muon_dedx->GetEntries();
     //double averageLA = m_histoTH1F["cellmuon_dedx_LayerA"]->Integral()/m_histoTH1F["cellmuon_dedx_LayerA"]->GetEntries();
     //double averageLBC = m_histoTH1F["cellmuon_dedx_LayerBC"]->Integral()/m_histoTH1F["cellmuon_dedx_LayerBC"]->GetEntries();
     //double averageLD = m_histoTH1F["cellmuon_dedx_LayerD"]->Integral()/m_histoTH1F["cellmuon_dedx_LayerD"]->GetEntries();

     double averageAll = hn_muon_dedx->GetMean();
     double averageLA = m_histoTH1F["cellmuon_dedx_LayerA"]->GetMean();
     double averageLBC = m_histoTH1F["cellmuon_dedx_LayerBC"]->GetMean();
     double averageLD = m_histoTH1F["cellmuon_dedx_LayerD"]->GetMean();

     //std::cout<<"checkAll: "<<averageAll<<"int: "<<hn_muon_dedx->Integral()<<" ent: "<<hn_muon_dedx->GetEntries()<<std::endl;
     //std::cout<<"checkLA: "<<averageLA<<"int: "<<m_histoTH1F["cellmuon_dedx_LayerA"]->Integral()<<" ent: "<<m_histoTH1F["cellmuon_dedx_LayerA"]->GetEntries()<<std::endl;
     //std::cout<<"checkLBC: "<<averageLBC<<"int: "<<m_histoTH1F["cellmuon_dedx_LayerBC"]->Integral()<<" ent: "<<m_histoTH1F["cellmuon_dedx_LayerBC"]->GetEntries()<<std::endl;
     //std::cout<<"checkLD: "<<averageLD<<"int: "<<m_histoTH1F["cellmuon_dedx_LayerD"]->Integral()<<" ent: "<<m_histoTH1F["cellmuon_dedx_LayerD"]->GetEntries()<<std::endl;

     //std::cout<<"checkAll: "<<averageAll<<std::endl;
     //std::cout<<"checkLA: "<<averageLA<<std::endl;
     //std::cout<<"checkLBC: "<<averageLBC<<std::endl;
     //std::cout<<"checkLD: "<<averageLD<<std::endl;


     for(int i = 0; i<All_dedx.size(); i++){
       h2AllTdEvscelleta->Fill(All_eta[i],All_dedx[i]/averageAll);
       h2AllTdEvscellphi->Fill(All_phi[i],All_dedx[i]/averageAll);
     }

     for(int i = 0; i<LA_dedx.size(); i++){
       h2LATdEvscelleta->Fill(LA_eta[i],LA_dedx[i]/averageLA);
       h2LATdEvscellphi->Fill(LA_phi[i],LA_dedx[i]/averageLA);
     }
     for(int i = 0; i<LBC_dedx.size(); i++){
       h2LBCTdEvscelleta->Fill(LBC_eta[i],LBC_dedx[i]/averageLBC);
       h2LBCTdEvscellphi->Fill(LBC_phi[i],LBC_dedx[i]/averageLBC);
     }
     for(int i = 0; i<LD_dedx.size(); i++){
       h2LDTdEvscelleta->Fill(LD_eta[i],LD_dedx[i]/averageLD);
       h2LDTdEvscellphi->Fill(LD_phi[i],LD_dedx[i]/averageLD);
     }





  outputFile->Write();
  outputFile->Close();
  std::cout<<"finished "<<std::endl;
  // Return gracefully:
  return 0;
}
